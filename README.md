# Terraform Examples

Collection of Terraform examples. Most of this examples cover an specific situation and are meant to be used as part of a larger codebase.

## [wait-for-asg](wait-for-asg/)

This example uses ASG lifecycle policies to block resource creation until the user data of ASG instances has been fully executed.

This is pretty useful for situations where later resources need to communicate or register with instances launched as part of an ASG, and we need to ensure Terraform does not continue launching resources until user data has been executed.