# wait-for-asg

## Introduction
This example code includes the minimum resources required to launch an ASG based on a Launch Configuration with a long running user data script. 

We also create an IAM Role with the minimum required permissions that the launched instances need to mark lifecycle actions as complete.

A standalone instance is also launched after the ASG finishes its deployment (with the use of `depends_on`), which will not happen until the ASG's user data finishes executing.

## Requisites
- Create a `terraform.tfvars` (template file provided under `terraform.tfvars.template`) file with the following variables:

```hcl
subnets = [
    "subnet-12345678",
    "subnet-abcdefgh"
]

autoscaling_group_name = "asg_name"
lifecycle_hook_name = "hook_name"
```

- Modify `main.tf` to configure region. 

- Configure your awscli with `aws configure` to provide access and secret keys to your AWS environment.

## Instructions

1) `terraform init`

2) `terraform plan -out=tfplan`

3) `terraform apply tfplan`
