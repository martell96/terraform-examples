########################################
#### --------- Providers --------- ##### 
########################################

provider "aws" {
# Configure region by changing and uncommenting following line
# region = "us-west-2"
}

########################################
#### -------- Data Sources -------- #### 
########################################

data "aws_region" "current" {}

data "aws_ami" "ubuntu_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-*"]
  }
}

########################################
#### --------- Resources --------- ##### 
########################################

resource "aws_iam_role" "allow_lifecycle_action" {
  name = "allow-lifecycle-action"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "allow_lifecycle_action" {
  name = "allow-lifecycle-action"
  role = "${aws_iam_role.allow_lifecycle_action.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Action": [
        "autoscaling:CompleteLifecycleAction"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "allow_lifecycle_action" {
  name = "allow-lifecycle-action"
  role = "${aws_iam_role.allow_lifecycle_action.name}"
}

resource "aws_launch_configuration" "ubuntu_slow" {
  name                 = "ubuntu-launch-configuration-slow"
  image_id             = "${data.aws_ami.ubuntu_ami.id}"
  instance_type        = "t2.micro"
  iam_instance_profile = "${aws_iam_instance_profile.allow_lifecycle_action.name}"

  user_data = <<EOF
#!/bin/bash -x

# Installing awscli as it is not included with Ubuntu
apt-get update && apt-get install awscli -y

# Simulating long user_data by using a sleep
sleep 120

# Notifying licefycle hook that execution of our user_data has been completed
aws autoscaling complete-lifecycle-action --lifecycle-action-result CONTINUE --instance-id $(curl -s http://169.254.169.254/latest/meta-data/instance-id) --lifecycle-hook-name ${var.lifecycle_hook_name} --auto-scaling-group-name ${var.autoscaling_group_name} --region ${data.aws_region.current.name}
EOF
}

resource "aws_autoscaling_group" "ubuntu_slow" {
  name                 = "${var.autoscaling_group_name}"
  max_size             = 1
  min_size             = 1
  launch_configuration = "${aws_launch_configuration.ubuntu_slow.name}"
  vpc_zone_identifier  = "${var.subnets}"

  initial_lifecycle_hook = {
    name                 = "${var.lifecycle_hook_name}"
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
  }

  tag = {
    key                 = "Name"
    value               = "Slow Instance"
    propagate_at_launch = true
  }
}

resource "aws_instance" "ubuntu_standalone" {
  ami           = "${data.aws_ami.ubuntu_ami.id}"
  instance_type = "t2.micro"

  tags = {
    Name = "Standalone Instance"
  }

  depends_on = ["aws_autoscaling_group.ubuntu_slow"]
}
