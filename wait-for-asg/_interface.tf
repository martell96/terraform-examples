variable "subnets" {
  description = "Subnet IDs for Autoscaling Group"
  type        = "list"
}

variable "autoscaling_group_name" {
  type = "string"
}

variable "lifecycle_hook_name" {
  type = "string"
}
